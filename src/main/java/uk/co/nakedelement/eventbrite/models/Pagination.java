package uk.co.nakedelement.eventbrite.models;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Pagination  implements Serializable
{
    private static final long serialVersionUID = -7047372074263754731L;
    
    @JsonProperty("object_count")
    private int count;
    
    @JsonProperty("page_number")
    private int pageNumber;
    
    @JsonProperty("page_size")
    private int pageSize;
    
    @JsonProperty("page_count")
    private int pageCount;
    
    @JsonProperty("continuation")
    private String continuation;
    
    @JsonProperty("has_more_items")
    private boolean hasMoreItems;
        
    public int getCount()
    {
        return count;
    }

    public int getPageNumber()
    {
        return pageNumber;
    }

    public int getPageSize()
    {
        return pageSize;
    }

    public int getPageCount()
    {
        return pageCount;
    }

    public String getContinuation()
    {
        return continuation;
    }

    public boolean isHasMoreItems()
    {
        return hasMoreItems;
    }   
}
