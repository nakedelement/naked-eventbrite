package uk.co.nakedelement.eventbrite.models;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Event implements Serializable
{
    private static final long serialVersionUID = -3466746339379269356L;
    
    private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
    
    @JsonProperty("id")
    private String id;
    
    @JsonProperty("name")
    private Element name;
    
    @JsonProperty("summary")
    private String summary;
    
    @JsonProperty("description")
    private Element description;
    
    @JsonProperty("url")
    private String url;
    
    @JsonProperty("vanity_url")
    private String vanityUrl;
    
    @JsonProperty("created")
    private String created;
    
    @JsonProperty("changed")
    private String changed;
    
    @JsonProperty("start")
    private EventDate start;
    
    @JsonProperty("end")
    private EventDate end;
    
    @JsonProperty("logo")
    private Logo logo;
    
    @JsonProperty("venue")
    private Venue venue;
    
    @JsonProperty("organization_id")
    private String organisationId;
        
    public String getId()
    {
        return id;
    }
    
    public String getName()
    {
        return name != null ? name.getText() : null;
    }
    
    public String getSummary()
    {
        return summary;
    }
    
    public String getDescription()
    {
        return description != null ? description.getHtml() : null;
    }
    
    public String getUrl()
    {
        if (vanityUrl != null)
            return vanityUrl;
        
        return url;
    }    
    
    public Date getCreated()
    {
        try
        {
            return sdf.parse(created);
        }
        catch (ParseException e)
        {
            return null;
        }
    }

    public Date getChanged()
    {
        try
        {
            return sdf.parse(changed);
        }
        catch (ParseException e)
        {
            return null;
        }
    }  
    
    public Date getStart()
    {
        return start.getLocal();
    }
    
    public Date getEnd()
    {
        return end.getLocal();
    }
    
    public String getImage()
    {
        return logo != null ? logo.getUrl() : null;
    }
    
    public String getVenue()
    {
        if (venue == null || venue.getName() == null)
            return null;
        
        final StringBuilder builder = new StringBuilder(venue.getName());
        if (venue.getAddress() != null)
        {
            builder.append(", ");
            builder.append(venue.getAddress());
        }
        return builder.toString();
    }
    
    public String getOrganisationId()
    {
        return organisationId;
    }

    @Override
    public String toString()
    {
        return getName();
    }

}
