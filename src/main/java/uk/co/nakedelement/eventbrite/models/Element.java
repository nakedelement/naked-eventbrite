package uk.co.nakedelement.eventbrite.models;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Element implements Serializable
{
    private static final long serialVersionUID = 5930505860490888658L;

    private String html;
    
    private String text;

    public String getHtml()
    {
        return html;
    }

    public String getText()
    {
        return text;
    }
    
    
}
