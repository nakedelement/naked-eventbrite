package uk.co.nakedelement.eventbrite.models;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Logo implements Serializable
{
    private static final long serialVersionUID = 7764226781317632200L;
    
    @JsonProperty("url")
    private String url;

    public String getUrl()
    {
        return url;
    }
}
