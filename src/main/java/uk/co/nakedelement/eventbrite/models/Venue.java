package uk.co.nakedelement.eventbrite.models;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Venue implements Serializable
{
    private static final long serialVersionUID = 884476474746753647L;
    
    @JsonProperty("name")
    private String name;
    
    @JsonProperty("address")
    private Address address;
    
    public String getName()
    {
        return name;
    }
    
    public String getAddress()
    {
        return address != null ? address.getAddress() : "";
    }
}
