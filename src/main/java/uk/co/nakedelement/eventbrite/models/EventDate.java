package uk.co.nakedelement.eventbrite.models;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EventDate implements Serializable
{
    private static final long serialVersionUID = -6629093610643174903L;
    
    private String local;
    
    private String timezone;
    
    private String utc;
    
    public Date getLocal()
    {
        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        try
        {
            return sdf.parse(local);
        }
        catch (ParseException e)
        {
            return null;
        }
    }
    
    public String getTimezone()
    {
        return timezone;
    }
    
    public Date getUtc()
    {
        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        try
        {
            return sdf.parse(utc);
        }
        catch (ParseException e)
        {
            return null;
        }
    }    
}
