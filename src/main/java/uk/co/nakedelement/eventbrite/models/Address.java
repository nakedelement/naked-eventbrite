package uk.co.nakedelement.eventbrite.models;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Address implements Serializable
{
    private static final long serialVersionUID = 6686923195255614362L;
    
    @JsonProperty("localized_address_display")
    private String address;
    
    public String getAddress()
    {
        return address;
    }

}
