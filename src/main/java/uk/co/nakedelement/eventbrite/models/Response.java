package uk.co.nakedelement.eventbrite.models;

import java.io.Serializable;
import java.util.Collection;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Response  implements Serializable
{
    private static final long serialVersionUID = -281254447281743401L;
    
    @JsonProperty("pagination")
    private Pagination pagination;
    
    @JsonProperty("events")
    private Collection<Event> events;

    public Pagination getPagination()
    {
        return pagination;
    }

    public Collection<Event> getEvents()
    {
        return events;
    }    
}
