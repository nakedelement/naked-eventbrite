package uk.co.nakedelement.eventbrite;

import java.io.IOException;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;

import uk.co.nakedelement.eventbrite.callback.EventCallback;
import uk.co.nakedelement.eventbrite.models.Event;
import uk.co.nakedelement.eventbrite.models.Response;

public class EventbriteClient
{
private static final Logger LOGGER = Logger.getLogger(EventbriteClient.class);
    
    private static final String MEETUP_URL = "https://www.eventbriteapi.com/v3/";
    
    private final String token;
    
    public EventbriteClient(String token)
    {
        this.token = token;
    }

    public void events(Collection<String> organisationIds, EventCallback eventCallback)
    {
        for(final String organisationId : organisationIds)
        {
            Response response = firstPage(organisationId);
            process(response, eventCallback);
            while(response.getPagination().isHasMoreItems())
            {
                response = nextPage(organisationId, response.getPagination().getContinuation());
                process(response, eventCallback);
            }            
        }
    }
    
    private void process(Response response, EventCallback eventCallback)
    {
        for(final Event event : response.getEvents())
            eventCallback.event(event);
    }   
    
    private Response firstPage(String organisationId)
    {
        return toObject(getFile(buildUrl(new StringBuilder("users/").append(organisationId).append("/events/").toString())), Response.class);
    }
    
    private Response nextPage(String organisationId, String continuation)
    {
        final Map<String, String> params = new HashMap<>();
        params.put("continuation", continuation);
        return toObject(getFile(buildUrl(new StringBuilder("users/").append(organisationId).append("/events/").toString(), params)), Response.class);
    }
    
    private URL buildUrl(String url)
    {
        return buildUrl(url, new HashMap<>());
    }
    
    private URL buildUrl(String url, Map<String, String> queryParams)
    {
        try
        {
              
            final StringBuilder builder = new StringBuilder(MEETUP_URL);
            builder.append(url);
            builder.append("?token=");
            builder.append(token);   
            builder.append("&expand=organizer,venue");
            
            for(Map.Entry<String, String> entry : queryParams.entrySet())
            {
                builder.append("&");
                builder.append(entry.getKey());
                builder.append("=");
                builder.append(entry.getValue());
            }            
            
            return new URL(builder.toString());
        }
        catch (MalformedURLException e)
        {
            throw new RuntimeException(e);
        }
    }
    
    private static String getFile(URL url)
    {
        try
        {
            LOGGER.debug(url);            
            final HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestMethod("GET");
            
            final StringWriter writer = new StringWriter();
            IOUtils.copy(conn.getInputStream(), writer, StandardCharsets.UTF_8);
            final String content = writer.toString();
                        
            return content;
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }
    
    private static <T> T toObject(String src, Class<T> valueType)
    {
        if (src == null)
        {
            LOGGER.warn("null source");
            return null;
        }

        try
        {
            final ObjectMapper mapper = new ObjectMapper();
            LOGGER.debug(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(src).replace("\\r\\n", "\r\n"));
            return mapper.readValue(src, valueType);
        }
        catch (IOException e)
        {
            LOGGER.warn(e);
            return null;
        }
    }
}
