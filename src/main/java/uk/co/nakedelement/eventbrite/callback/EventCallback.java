package uk.co.nakedelement.eventbrite.callback;

import uk.co.nakedelement.eventbrite.models.Event;

public interface EventCallback
{
    void event(Event event);
}
