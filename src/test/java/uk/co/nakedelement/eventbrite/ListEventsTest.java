package uk.co.nakedelement.eventbrite;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Properties;

import org.junit.Before;
import org.junit.Test;

import uk.co.nakedelement.eventbrite.callback.EventCallback;
import uk.co.nakedelement.eventbrite.models.Event;

public class ListEventsTest
{
    private EventbriteClient svc;

    @Before
    public void setUp() throws Exception
    {
        try(final InputStream is = new FileInputStream(new File(System.getProperty("user.home") + "/naked-eventbrite.properties")))
        {
            final Properties props = new Properties();
            props.load(is);            
            svc = new EventbriteClient(props.getProperty("token"));
        }
        
    }

    @Test
    public void events()
    {
        svc.events(Arrays.asList("3405714280"), new EventCallback()
        {
            @Override
            public void event(Event event)
            {
                System.out.println(event.getId());
                System.out.println(event.getName());
                System.out.println(event.getDescription());
                System.out.println(event.getSummary());
                System.out.println(event.getUrl());
                System.out.println(event.getStart());
                System.out.println(event.getEnd());
                System.out.println(event.getCreated());
                System.out.println(event.getChanged());
                System.out.println(event.getImage());
                System.out.println(event.getVenue());
                System.out.println(event.getOrganisationId());
                System.out.println("");
            }    
        });
    }

}
